package com.readernews;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.readernews.data.CategoryData;
import com.readernews.data.ListData;
import com.readernews.utils.Utils;

public class Main extends Activity 
{
	private final String TAG = this.getClass().getSimpleName();
	private Context context = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_main);
		context = this;
		
		
		
				
		Button btnFetchNews = (Button)	findViewById(R.id.btnFetchNews);
		btnFetchNews.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT).show();
				//call intentservice and set resultreceiver
				MyResultReceiver theReceiver = new MyResultReceiver(new Handler());
		        theReceiver.setParentContext(context);
		        Intent i = new Intent(context, MyIntentService.class);
		        i.putExtra("url", context.getString(R.string.urlCategories));
		        i.putExtra("resReceiver", theReceiver);
		        startService(i);
		        

			}
		});
	}

	
	
	public class MyResultReceiver extends ResultReceiver {

	    private Context context = null;

	    protected void setParentContext (Context context) {
	        this.context = context;
	    }

	    public MyResultReceiver(Handler handler) {
	        super(handler);
	    }

	    @Override
	    protected void onReceiveResult (int resultCode, Bundle resultData) {

	    	if(resultCode == 1)
	    	{
	    		Utils.printDebug(TAG, "result received");
	    		ListData l = (ListData) resultData.getParcelable("data");
	    		ArrayList<CategoryData> list = l.getListCategoryData();
	    		setContentView(R.layout.layout_list);
	    		ListView listView = (ListView) 	findViewById(R.id.list);
	    		listView.setAdapter(new EfficientAdapter(context, list));
	    	
	    	}
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class EfficientAdapter extends BaseAdapter
	{
		ArrayList<CategoryData> list;
		private LayoutInflater mInflater;
		
		public EfficientAdapter(Context context, ArrayList<CategoryData> list)
		{
			mInflater = LayoutInflater.from(context);
			this.list = list;
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int pos) {
			// TODO Auto-generated method stub
			return pos;
		}

		@Override
		public long getItemId(int pos) {
			// TODO Auto-generated method stub
			return pos;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			ViewHolder holder;

			if (convertView == null) 
			{
				convertView = mInflater.inflate(R.layout.list_item, null);
				holder = new ViewHolder();
				
				holder.txtId						= (TextView) convertView.findViewById(R.id.txtId);
				holder.txtDisplayName				= (TextView) convertView.findViewById(R.id.txtDisplayName);
				holder.txtEnglishName				= (TextView) convertView.findViewById(R.id.txtEnglishName);
				holder.txtUrl						= (TextView) convertView.findViewById(R.id.txtUrl);
				
				
				convertView.setTag(holder);
			} else 
			{
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.txtId.setText(Integer.toString(list.get(position).getId()));
			holder.txtDisplayName.setText(list.get(position).getDisplayName());
			holder.txtEnglishName.setText(list.get(position).getEnglishName());
			holder.txtUrl.setText(list.get(position).getUrl());
						
			return convertView;
		}
	}
	
	private static class ViewHolder 
	{
		TextView txtId;
		TextView txtDisplayName;
		TextView txtEnglishName;
		TextView txtUrl;
	}

		
	
}
 