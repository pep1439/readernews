package com.readernews.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.readernews.data.CategoryData;

import android.util.Log;

public class Utils 
{
	/**
	 * used to print debug
	 * @param tag
	 * @param msg
	 */
	public static void printDebug(String tag, String msg)
	{
		Log.d(tag, msg);
	}
	
	/**
	 * used to print error
	 * @param tag
	 * @param msg
	 */
	public static void printError(String tag, String msg)
	{
		Log.e(tag, msg);
	}

	/**
	 * used to read data from given url
	 * @param strUrl - JSON url
	 * @return
	 */
	public static String readFromURL(String strUrl) 
	{
		StringBuilder sb = new StringBuilder();
		try {
			
            BufferedReader reader = new BufferedReader(new InputStreamReader(
            		new URL(strUrl).openStream(), "iso-8859-1"), 8);
            
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            
        } catch (Exception e) {
            printError("Utils.readFromURL()", "Error converting result " + e.toString());
        }
		return sb.toString();
	}

	/**
	 * used to parse category JSON
	 * @param response - JSON
	 * @return ArrayList<CategoryData>
	 */
	public static ArrayList<CategoryData> parseCategoryJSON(String response) 
	{
		ArrayList<CategoryData> listCategoryData = new ArrayList<CategoryData>();
		
		try
		{
			//JSONObject json = new JSONObject(response);
			JSONArray ary = new JSONArray(response);
			for(int index = 0; index < ary.length(); index++)
			{
				JSONObject obj = ary.getJSONObject(index);
				
				CategoryData cData = new CategoryData();
				cData.setId(obj.getInt("category_id"));
				cData.setDisplayName(obj.getString("display_category_name"));
				cData.setEnglishName(obj.getString("english_category_name"));
				cData.setUrl(obj.getString("url_category_name"));
				listCategoryData.add(cData);
				
				//printDebug("", cData.getEnglishName());
			}
		}
		catch(Exception e)
		{
			printError("Utils.parseCategoryJSON()", "Error : " + e.toString());
		}
		
		
		
		return listCategoryData;
	}
}
