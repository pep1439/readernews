package com.readernews.data;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * used to save category record
 * @author 
 *
 */
public class CategoryData implements Parcelable
{
	private int id;
	private String displayName;
	private String englishName;
	private String url;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	@Override
	public int describeContents() 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(displayName);
		dest.writeString(englishName);
		dest.writeString(url);
	}
	
	private void readFromParcel(Parcel in) 
	{
		id = in.readInt();
		displayName = in.readString();
		englishName = in.readString();
		url = in.readString();
		
		
	}
	
	public CategoryData(Parcel in) 
	{ 
		readFromParcel(in); 
	}
	
		
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() 
	{
		public CategoryData createFromParcel(Parcel in) 
		{ 
			return new CategoryData(in); 
		}   
		public CategoryData[] newArray(int size) 
		{ 
			return new CategoryData[size]; 
		}
	}; 
	
	public CategoryData() {  };  
	
}
