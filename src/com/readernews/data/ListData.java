package com.readernews.data;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * used to save list record and its parcelable 
 * @author 
 *
 */
public class ListData implements Parcelable
{
	private ArrayList<CategoryData> listCategoryData;
	
	
	
	
	public ArrayList<CategoryData> getListCategoryData() {
		return listCategoryData;
	}

	public void setListCategoryData(ArrayList<CategoryData> listCategoryData) {
		this.listCategoryData = listCategoryData;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		// TODO Auto-generated method stub
		dest.writeTypedList(listCategoryData);
	}
	
	private void readFromParcel(Parcel in) 
	{
		in.readTypedList(listCategoryData, CategoryData.CREATOR);
		
	}
	
	public ListData(Parcel in) 
	{ 
		readFromParcel(in); 
	}
	
		
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() 
	{
		public ListData createFromParcel(Parcel in) 
		{ 
			return new ListData(in); 
		}   
		public ListData[] newArray(int size) 
		{ 
			return new ListData[size]; 
		}
	}; 
	
	public ListData() {  };  


}
