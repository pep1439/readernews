package com.readernews;

import java.util.ArrayList;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.readernews.data.CategoryData;
import com.readernews.data.ListData;
import com.readernews.utils.Utils;

public class MyIntentService extends IntentService
{
	private final String TAG = this.getClass().getSimpleName();
	Main ctx=new Main();
	public MyIntentService(String name) {
		super(name);
		Utils.printDebug(TAG, "constructor called.");
		// TODO Auto-generated constructor stub
	}
	
	public MyIntentService()
	{
		super("constructor");
	}

	@Override
    public void onCreate() {
        super.onCreate();
        Utils.printDebug(TAG, "onCreate called.");
	}
	
	@Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
		Utils.printDebug(TAG, "onStartCommand called.");
		
		return super.onStartCommand(intent,flags,startId);
	  }
	
	
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Utils.printDebug(TAG, "onHandleIntent called.");
		
		ResultReceiver resRec = intent.getParcelableExtra("resReceiver");

		//
		Bundle resultData = new Bundle();
		
		ListData l = new ListData();
		l.setListCategoryData(fetchNews(this));
		
		resultData.putParcelable("data", l);

		//send callback
		resRec.send(1, resultData);
		
	}
	
	/**
	 * method used to fetch the news from JSON
	 * @param strUrl JSON url
	 */
	public ArrayList<CategoryData> fetchNews(Context context) 
	{
		String str=context.getString(R.string.urlCategories);
		Utils.printDebug(TAG, "Fetching News");
		
		String response = Utils.readFromURL(str);
		return Utils.parseCategoryJSON(response);
		

	}


}
